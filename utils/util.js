function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function showMsg(msg) {
  wx.showToast({
    title: msg,
    icon: 'loading',
    duration: 1500
  })
}
function showMod(msg) {
  wx.showModal({
    title: '提示',
    content: msg,
    success: function (rescc) {
      if (rescc.confirm) {
        //that.getUserInfo();
      }
    }
  })
}
function urlBack(num) {
  wx.navigateBack({
    delta: num
  })
}
function getRand(min, max) {
  return parseInt(Math.random() * (max - min + 1) + min, 10);
}
module.exports = {
  formatTime: formatTime,
  showMsg: showMsg,
  showMod: showMod,
  urlBack: urlBack,
  getRand: getRand
}
