var util = require('../../../utils/util.js');
var app = getApp();
Page({
   data: {
      height: '1000',
      img: ''
   },
   onShow: function () {
      this.getYuejinbang();
   },
   getYuejinbang: function () {
      var that = this;
      util.showMsg('加载中...');
      wx.request({
         url: app.globalData.url + 'Ydrundata/getYuejinbang',
         method: "POST",
         header: app.globalData.header,
         fail: function (res) {
            wx.stopPullDownRefresh() //停止下拉刷新
            util.showMsg('获取数据失败' + res);
         },
         success: function (res) {
            wx.hideToast();
            //wx.hideNavigationBarLoading() //完成停止加载
            wx.stopPullDownRefresh() //停止下拉刷新
            that.setData({
               img: res.data.yuejinbangimg,
               height: res.data.yuejinbangheight,
            });
         }
      })
   }
})