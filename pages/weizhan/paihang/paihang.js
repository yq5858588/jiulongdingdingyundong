var util = require('../../../utils/util.js');
var app = getApp();
Page({
  data: {
     paihanglist: [],
     page: 1,
     more: true,
  },
  onShow: function () {
     console.log('paihangonShow')
     var that = this;
     var sessionid = wx.getStorageSync('sessionid');
     var userInfo = wx.getStorageSync('userInfo');
     var runDataYesterday = wx.getStorageSync('runDataYesterday');
     var dingtotal = wx.getStorageSync('dingtotal');
     var rundatapeizhi = wx.getStorageSync('rundatapeizhi');
     var getdingFlag = wx.getStorageSync('getdingFlag');
     that.setData({
        sessionid: sessionid,
        userInfo: userInfo,
        dingtotal: dingtotal,
        level: userInfo.level,
        rundatapeizhi: rundatapeizhi,
        getdingFlag: getdingFlag,
        runDataYesterday: runDataYesterday
     });
     that.getPaihang();
  },
  onReachBottom: function () {
     var that = this;
     var that = this;
     if (that.data.more) {
        that.getPaihang();
     }
  },
  getPaihang: function (){
     var that = this;
     var sessionid = wx.getStorageSync('sessionid');
     var options=[];
     options['sessionid'] = sessionid;
     options['page'] = that.data.page;
     options['rows'] = 5;
     //console.log(options);
     util.showMsg('加载中...');
     wx.request({
        url: app.globalData.url + 'Ydrundata/getPaihang',
        method: "POST",
        data: options,
        header: app.globalData.header,
        fail: function (res) {
           wx.stopPullDownRefresh() //停止下拉刷新
           util.showMsg('获取数据失败' + res);
        },
        success: function (res) {
           wx.hideToast();
           //wx.hideNavigationBarLoading() //完成停止加载
           wx.stopPullDownRefresh() //停止下拉刷新
           var l = that.data.paihanglist;
           for (var i = 0; i < res.data.length; i++) {
              l.push(res.data[i])
           }
           var options = that.data.options;
           var page = that.data.page;
           page++;
           that.setData({
              paihanglist: l,
              page: page,
           });
           if (res.data.length == 0) {
              that.setData({
                 more: false
              });
           }
        }
     })
  }
})
