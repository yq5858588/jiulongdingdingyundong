var util = require('../../../utils/util.js');
var app = getApp();
Page({
   data: {
      row: [],
   },
   onShow: function () {
      this.getPaihang();
   },
   getPaihang: function () {
      var that = this;
      var sessionid = wx.getStorageSync('sessionid');
      var options = [];
      options['sessionid'] = sessionid;
      //console.log(options);
      util.showMsg('加载中...');
      wx.request({
         url: app.globalData.url + 'Ydrundata/getUserprize',
         method: "POST",
         data: options,
         header: app.globalData.header,
         fail: function (res) {
            wx.stopPullDownRefresh() //停止下拉刷新
            util.showMsg('获取数据失败' + res);
         },
         success: function (res) {
            wx.hideToast();
            //wx.hideNavigationBarLoading() //完成停止加载
            wx.stopPullDownRefresh() //停止下拉刷新
            that.setData({
               row: res.data,
            });
         }
      })
   }
})
