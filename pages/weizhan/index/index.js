var util = require('../../../utils/util.js');
var app = getApp();
Page({
  data: {
    piaofutext: [],
    sessionid: '',
    runDataYesterday: '',
    userInfo: {},
    dingtotal: 0,
    level: 0,
    rundatapeizhi: [],
    getding: 'getding',
    ding: "ding.gif",
    getdingFlag: false,
    backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg0.jpg",
    leveltip: 'level1_1l.png',
    piaofu1: 'hidden',
    piaofu2: 'hidden',
    getprize: 'hidden',
    piaofutext1: '',
    piaofutext2: ''
  },
  onShareAppMessage: function (res) {
    var that = this;
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '九龙丁丁运动',
      path: '/pages/weizhan/loading/loading',
      success: function (res) {
        // 转发成功
        util.showMsg('分享成功正在获得奖励...');
        wx.request({
          url: app.globalData.url + 'Ydrundata/fenxiang',
          method: "POST",
          data: { sessionid: that.data.sessionid, dingtype: '分享' },
          header: app.globalData.header,
          fail: function (res) {
            wx.stopPullDownRefresh() //停止下拉刷新
            util.showMsg('获取数据失败' + res);
          },
          success: function (res) {
            wx.hideToast();
            //wx.hideNavigationBarLoading() //完成停止加载
            wx.stopPullDownRefresh() //停止下拉刷新
            console.log(res);
            wx.setStorageSync('dingtotal', res.data.dingtotal);
            util.showMod(res.data.info);
            that.setData({
              dingtotal: res.data.dingtotal,
              level: res.data.userinfo.level
            });
          }
        })
      },
      fail: function (res) {
        // 转发失败
      }
    }
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    console.log('indexonShow')
    var that = this;
    var sessionid = wx.getStorageSync('sessionid');
    var userInfo = wx.getStorageSync('userInfo');
    var runDataYesterday = wx.getStorageSync('runDataYesterday');
    var dingtotal = wx.getStorageSync('dingtotal');
    var rundatapeizhi = wx.getStorageSync('rundatapeizhi');
    var piaofutext = wx.getStorageSync('piaofutext');
    var getdingFlag = wx.getStorageSync('getdingFlag');
    that.setData({
      sessionid: sessionid,
      userInfo: userInfo,
      dingtotal: dingtotal,
      level: userInfo.level,
      rundatapeizhi: rundatapeizhi,
      piaofutext: piaofutext,
      getdingFlag: getdingFlag,
      runDataYesterday: runDataYesterday,
    });
    if (getdingFlag == false) {
      that.setData({
        getding: 'getdingh'
      });
    }
    if (userInfo.level < 1) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg0.jpg",
        leveltip: 'level1_1l.png'
      });
    } else if (2 > 2 > userInfo.level && userInfo.level >= 1) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg1.jpg",
        leveltip: 'level1_1l.png'
      });
    } else if (3 > userInfo.level && userInfo.level >= 2) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg2.jpg",
        leveltip: 'level1_2l.png'
      });
    } else if (4 > userInfo.level && userInfo.level >= 3) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg3.jpg",
        leveltip: 'level1_3l.png',
        ding: 'ding8.gif'
      });
    } else if (5 > userInfo.level && userInfo.level >= 4) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg4.jpg",
        leveltip: 'level1_4l.png',
        ding: 'ding8.gif'
      });
    } else if (6 > userInfo.level && userInfo.level >= 5) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg5.jpg",
        leveltip: 'level1_5l.png',
        ding: 'ding8.gif'
      });
    } else if (7 > userInfo.level && userInfo.level >= 6) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg6.jpg",
        leveltip: 'level1_6l.png',
        ding: 'ding8.gif'
      });
    } else if (8 > userInfo.level && userInfo.level >= 7) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg7.jpg",
        leveltip: 'level1_7l.png',
        ding: 'ding10.gif'
      });
    } else if (9 > userInfo.level && userInfo.level >= 8) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg8.jpg",
        leveltip: 'level1_8l.png',
        ding: 'ding10.gif'
      });
    } else if (10 > userInfo.level && userInfo.level >= 9) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg9.jpg",
        leveltip: 'level1_9l.png',
        ding: 'ding10.gif'
      });
    } else if (11 > userInfo.level && userInfo.level >= 10) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg10.jpg",
        leveltip: 'level1_10l.png',
        ding: 'ding14.gif'
      });
    } else if (12 > userInfo.level && userInfo.level >= 11) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg11.jpg",
        leveltip: 'level1_11l.png',
        ding: 'ding14.gif'
      });
    } else if (13 > userInfo.level && userInfo.level >= 12) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg12.jpg",
        leveltip: 'level1_12l.png',
        ding: 'ding14.gif'
      });
    } else if (14 > userInfo.level && userInfo.level >= 13) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg13.jpg",
        leveltip: 'level1_13l.png',
        ding: 'ding14.gif'
      });
    } else if (15 > userInfo.level && userInfo.level >= 14) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg14.jpg",
        leveltip: 'level1_14l.png',
        ding: 'ding14.gif'
      });
    } else if (userInfo.level && userInfo.level >= 15) {
      that.setData({
        backgroundImg: "https://weixin.xajlnkyjy.com/tp323/Jishu/Yundong/Images/level1-bg14.jpg",
        leveltip: 'level1_14l.png',
        ding: 'ding14.gif'
      });
    }
  },
  getDing: function (option) {
    var that = this;
    util.showMsg('加载中...');
    wx.request({
      url: app.globalData.url + 'Ydrundata/getDing',
      method: "POST",
      data: { sessionid: that.data.sessionid, step: option.currentTarget.dataset.rundata, dingtype: '步数' },
      header: app.globalData.header,
      fail: function (res) {
        wx.stopPullDownRefresh() //停止下拉刷新
        util.showMsg('获取数据失败' + res);
      },
      success: function (res) {
        wx.hideToast();
        //wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新
        console.log(res);
        wx.setStorageSync('dingtotal', res.data.dingtotal);
        util.showMod(res.data.info);
        that.setData({
          dingtotal: res.data.dingtotal,
          level: res.data.userinfo.level,
          getding: 'getdingh'
        });
        if (res.data.userinfo.level == 4 || res.data.userinfo.level == 6 || res.data.userinfo.level == 8 || res.data.userinfo.level == 10 || res.data.userinfo.level == 12 || res.data.userinfo.level == 14) {
          that.setData({
            level: res.data.userinfo.level,
            getprize: 'visible',
          });
        }
      }
    })
  },
  dingClick: function () {
    var that = this;
    wx.vibrateLong();
    that.setData({
      piaofu1: 'hidden',
      piaofu2: 'hidden',
    });
    var randtext = util.getRand(0, that.data.piaofutext.length - 1);
    var randpiaofu = util.getRand(1, 2);
    if (randpiaofu == 1) {
      that.setData({
        piaofu1: 'visible',
        piaofutext1: that.data.piaofutext[randtext]
      });
    } else {
      that.setData({
        piaofu2: 'visible',
        piaofutext2: that.data.piaofutext[randtext]
      });
    }

  }
})
